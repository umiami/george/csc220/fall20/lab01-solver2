[![pipeline status](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/badges/master/pipeline.svg)](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/-/pipelines?ref=master)
[![compile](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/fall20/lab01-solver2/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Lab01: Introduction

> This project is designed to introduce programing environment to students. It prints out `Hello world!` string and simulates a coin flip with an uneven probability. Additionally, it analyzes arrays of integers searching for specific value pairs.

- For the sake of this lab **stick to the GitLab browser UI**.
- This assignment is completed when all tests are passing.

Before you proceed to instructions, make sure you have walked through the [AMS Tutorial for Solvers](https://docs.google.com/document/d/1UrVbwJ5e2kDnNlxiN3vVqeRovPBC4NF0TDmf4guMmSY/edit?usp=sharing). Always make sure there is **no pending merge request**!

## Instructions: lab part

1. Use **GitLab browser UI** editor.
   1. Navigate to the source file in `./src/main/Introduction.java`.
   2. Take your time to explore the source file.
   3. Click Edit button to start editing.
1. Print "Hello world!" from the `main` method.
   1. Replace the first `TODO` statement in `main` method with the following line of code. Use copy and paste.
      ```java
      System.out.println("Hello world!");
      ```
   1. Double-check your modifications before committing by clicking on the Preview changes button in the editor’s header.
   1. Describe what the current modification does into a commit message (you may copy and paste the current instruction) and press Commit changes.
   1. Make sure the _first_ test is passing. After pipeline stops, the test passed badge will show `1/4`.
1. Implement the `coinFlip` method.
   1. Replace the `TODO` statement in `coinFlip` method with the following block of code. Use copy and paste and do not forget to remove the redundant empty return statement.
      ```java
      if (Math.random() < 0.55) {
        return "heads";
      }
      return "tails";
      ```
   1. Double-check your modifications before committing by clicking on the Preview changes button in the editor’s header.
   1. Describe what the current modification does into a commit message (you may copy and paste the current instruction) and press Commit changes.
   1. Make sure _both first and second_ tests are passing. After pipeline stops, the test passed badge will show `2/4`.
1. Evaluate 100 runs of `coinFlip` in the `main` method.
   1. Replace the second `TODO` statement in `main` method with the following block of code. Use copy and paste.
      ```java
      int heads = 0;
      for (int i = 0; i < 100; i++) {
        if (coinFlip() == "heads") {
          heads++;
        }
      }
      System.out.println(heads);
      ```
   1. Double-check your modifications before committing by clicking on the Preview changes button in the editor’s header.
   1. Describe what the current modification does into a commit message (you may copy and paste the current instruction) and press Commit changes.
   1. Make sure _first three_ tests are passing. After pipeline stops, the test passed badge will show `3/4`.

## Instructions: assignment part

1. Implement the `checkSum` method according to its Javadoc description.
1. Repeat the commit routine from above: double-check, describe and commit.
1. Make sure _all tests_ are passing. After pipeline stops, the test passed badge will show `4/4`.

## Bonus questions

1. You may want to boost your coding style to 100%. Can you figure out how?
1. Why is coding style important? See [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) and the internet.
1. What is the (expected) complexity of your `checkSum` implementation?
1. What is the best `checkSum` complexity you can think of? How about *O(n)*?
1. Can you think of an *O(n)* algorithm that would work for _unsorted_ arrays?
